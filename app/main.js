require.config({
    urlArgs: "bust=" + (new Date()).getTime(),
    paths: {
        'angular': 'bower_components/angular/angular',
        'ngCookies': 'bower_components/angular-cookies/angular-cookies',
        'jquery' : 'bower_components/jquery/dist/jquery',
        'jquery-placeholder' : 'bower_components/jquery-placeholder/jquery.placeholder',
        'bootstrap' :  'bower_components/bootstrap/dist/js/bootstrap',
        'ui.router': 'bower_components/angular-ui-router/release/angular-ui-router',
        'ui.bootstrap': 'bower_components/angular-bootstrap/ui-bootstrap',
        'ui.bootstrap.tpls': 'bower_components/angular-bootstrap/ui-bootstrap-tpls',
        'ngSanitize': 'bower_components/angular-sanitize/angular-sanitize',
        'ui.select': 'bower_components/ui-select/dist/select',
        'pascalprecht.translate': 'bower_components/angular-translate/angular-translate',
        'pascalprecht.translate.static.files': 'bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files',
        'pascalprecht.translate.storage.cookie': 'bower_components/angular-translate-storage-cookie/angular-translate-storage-cookie',
        'pascalprecht.translate.storage.local': 'bower_components/angular-translate-storage-local/angular-translate-storage-local',
        'animatescroll': 'bower_components/animatescroll/animatescroll',
        'project': 'js/pages/project',

        // Angular Google Maps
        //'lodash': 'bower_components/lodash/lodash',
        //'nemLogging': 'bower_components/angular-simple-logger/dist/angular-simple-logger',
        //'uiGmapgoogle-maps': 'bower_components/angular-google-maps/dist/angular-google-maps'
    },
    shim: {
        'angular': {
            exports: 'angular'
        },
        'ngCookies': {
            deps: ['angular']
        },
        'bootstrap': {
            deps: ['jquery']
        },
        'animatescroll':{
            deps: ['jquery']
        },
        'project':{
            deps: ['jquery', 'angular', 'animatescroll', 'ui.bootstrap.tpls']
        },
        'jquery-placeholder':{
            deps: ['jquery']
        },
        'ui.router': {
            deps: ['angular']
        },
        'ui.bootstrap': {
            deps: ['angular', 'bootstrap']
        },
        'ui.bootstrap.tpls': {
            deps: ['ui.bootstrap']
        },
        'ui.select': {
            deps: ['angular']
        },
        'ngSanitize':{
            deps: ['angular']
        },
        'pascalprecht.translate.static.files': {
            deps: ['angular', 'pascalprecht.translate'],
            exports: 'pascalprecht.translate'
        },
        'pascalprecht.translate.storage.cookie': {
            deps: ['angular', 'ngCookies', 'pascalprecht.translate'],
            exports: 'pascalprecht.translate'
        },
        'pascalprecht.translate.storage.local': {
            deps: ['angular', 'pascalprecht.translate.storage.cookie'],
            exports: 'pascalprecht.translate'
        },
        'pascalprecht.translate': {
            deps: ['angular'],
            exports: 'pascalprecht.translate'
        },

        // Angular Google Maps
        //'nemLogging': ['angular'],
        //'uiGmapgoogle-maps': ['angular', 'nemLogging', 'lodash'],
    }
});

//http://code.angularjs.org/1.2.1/docs/guide/bootstrap#overview_deferred-bootstrap
window.name = "NG_DEFER_BOOTSTRAP!";

require([
    'angular',
    'app'
], function(angular, app) {
    angular.element().ready(function() {
        angular.resumeBootstrap([app.name]);
    });
});