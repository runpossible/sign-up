define([
	'angular',
    'require',
    'pascalprecht.translate',
    'pascalprecht.translate.static.files',
    'pascalprecht.translate.storage.cookie',
    'pascalprecht.translate.storage.local',
    'jquery-placeholder',
    'animatescroll',
    'project',
    'ngCookies',
    'ui.router',
    'ui.bootstrap',
    'ui.bootstrap.tpls',
    'ngSanitize',
    'ui.select',
	'./js/home/home',
    'js/pages/pages',
    './config'
], function(angular, require) {
    var runApp = angular.module('runApp', [
        'pascalprecht.translate',
        'ngCookies',
        'ui.router',
        'ui.bootstrap',
        'ui.bootstrap.tpls',
        'ngSanitize',
        'ui.select',
        'home',
        'pages'
    ]);

    var appConfig = require('./config');

    runApp.constant('appConfig', appConfig);

    runApp.config(['$translateProvider', '$locationProvider', function ($translateProvider, $locationProvider) {
        $translateProvider.useStaticFilesLoader({
            prefix: 'languages/',
            suffix: '.json'
        });

        $translateProvider.useSanitizeValueStrategy(null);
        //$translateProvider.useLocalStorage();

        $translateProvider.preferredLanguage("bg");

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    }]);

    runApp.run(['$rootScope', '$window', '$state', function($rootScope, $window, $state) {
        $rootScope.$on('$stateChangeSuccess', function() {
                setTimeout(function(){
                    $('input, textarea').placeholder({ customClass: 'my-placeholder' });
                }, 300);
            });

    }]);

    return runApp;
});
