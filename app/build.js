({
    baseUrl: ".",
    name: "main",
    mainConfigFile: "main.js",
    out: "main-built.js",
    optimize: 'uglify2',
    uglify2: {
        mangle: false
    },
    wrapShim: true,
    waitSeconds: 0,
    skipDirOptimize: true,
    removeCombined: true,
    preserveLicenseComments: true
})