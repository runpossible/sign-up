define([], function () {
    'use strict';

    return ['$scope', '$http', '$translate', function($scope, $http, $translate) {
        $scope.subscription = { city: '0', newsletters: true};

        $http.get('/cities').then(function(result){
            $scope.cities = result.data;
            //$translate('CITY').then(function (translated) {
            //    result.data.unshift(translated + '*');
            //    $scope.cities = result.data;
            //    $scope.subscription.city = translated + '*';
            //});

        }, function(err){
            console.log(err);
        });

        $scope.signUp = function(subscription)
        {
            if(!$scope.signUpForm.$valid || !$scope.subscription.plays || !$scope.subscription.city) {
                $scope.missing_fields = true;
                return;
            }

            $scope.missing_fields = false;
            $http.post('/subscribe', subscription).then(function(){
                $scope.subscriptionReceived = true;
            }, function(err){
                console.log(err);
            });
        };


    }];
});
