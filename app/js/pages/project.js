$( document ).ready(function() {
    $(window).scroll(function(){
        if($('#header').isOnScreen() == true){
            $("#nav-anchor-points a").removeClass('active-section');
            $("#section-1").addClass('active-section');
            $(".wrapper-anchor").addClass('anchor-white');
        }
        if($('#workflow').isOnScreen() == true){
            $("#nav-anchor-points a").removeClass('active-section');
            $("#section-2").addClass('active-section');
            $(".wrapper-anchor").removeClass('anchor-white');
        }
        if($('#sign-up-form').isOnScreen() == true){
            $("#nav-anchor-points a").removeClass('active-section');
            $("#section-3").addClass('active-section');
            $(".wrapper-anchor").removeClass('anchor-white');
        }
    })

});


$.fn.isOnScreen = function(){

    var win = $(window);

    var viewport = {
        top : win.scrollTop(),
        left : win.scrollLeft()
    };
    viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height();

    var bounds = this.offset();
    bounds.right = bounds.left + this.outerWidth();
    bounds.bottom = bounds.top + this.outerHeight();

    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));

};