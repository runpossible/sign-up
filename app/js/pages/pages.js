define(function(require) {
    var angular = require('angular');

    var pages = angular.module('pages', ['ui.router']);
    var signUpCtrl = require('/js/pages/sign-up.js');
    var thankYouCtrl = require('/js/pages/thank-you.js');

    pages.controller('SignUpCtrl', signUpCtrl);
    pages.controller('ThankYouCtrl', thankYouCtrl);

    pages.config(['$stateProvider', function($stateProvider) {
        $stateProvider.state('base.signup', {
            url: '',
            templateUrl: '/js/pages/sign-up.html',
            controller: 'SignUpCtrl'
        }).state('base.thankYou', {
                url: 'thank-you',
                templateUrl: '/js/pages/thank-you.html',
                controller: 'ThankYouCtrl'
            });
    }]);

    return pages;
});
