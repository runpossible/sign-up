define(['jquery'], function($) {
  'use strict';

  return ['$parse', '$translate', '$http', function($parse, $translate, $http) {
    return {
      restrict: 'E',
      templateUrl: 'js/common/country-select.html',
      replace: true,
      scope: {
        selectedCountryId: '='
      },
      link: function($scope) {
        var lang = $translate.use();

        $http.get('languages/countries.json').then(
          function(result) {
            var countries = [];
            var data = result.data;
            for(var i=0;i<data.length;i++) {
              countries.push({
                id: data[i].id,
                name: data[i][lang]
              });
            }

            $scope.countries = countries;
            if(countries.length > 0){
              $scope.selectedCountryId = countries[0].id;
            }
          }, function(err) {
            // todo; error handling, global.
          });
      }
    };
  }]
});