define(function(require) {
	var angular = require('angular');

	var home = angular.module('home', ['ui.router']);
    var countrySelectDir = require('/js/common/country-select.js');

    home.directive('countrySelector', countrySelectDir);

	home.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');

        // This is the base template, it cannot be viewed - it's abstract.
        $stateProvider.state('base', {
                url: '/',
                abstract: true,
                templateUrl: './js/home/home.html'
            })
            .state("otherwise", { 
                url : '/otherwise',
                templateUrl: './js/home/home.html'
            });
    }]);

    return home;
});
