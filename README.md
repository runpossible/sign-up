# FGO #

## Prerequisites  
 * MongoDB should be installed on your machine
 * you may need to install python before running npm install

## Setup

    cd /app
    bower install
    cd ../..
    npm install
    node index.js

Go to http://localhost:3000

## Directory Layout

    index.js --> the Node JS server, used only for serving statically the website assets
    app/ --> the web application resides here
        app.js --> setup of the angular application and modules
        main.js --> manually bootstrap the angular application, after configuring and loading the extenral modules with require js
        index.html --> the SPA application's page
        partials/
            main.html --> a partial view, rendered in <div ui-view> when accessing / and matching the "" route defined in the $stateProvider
