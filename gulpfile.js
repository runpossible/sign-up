/*
// Step 1: Install gulp globally
sudo npm install -g gulp

// Step 2: Install gulp in your project
npm install --save-dev gulp gulp-sass gulp-plumber

*/

var gulp = require('gulp');
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');

gulp.task('sass', function() {
  gulp.src('app/stylesheets/*.scss')
    .pipe(plumber())
    .pipe(sass())
    .pipe(gulp.dest('app/stylesheets'));
});

gulp.task('watch', function() {
  gulp.watch('app/stylesheets/*.scss', ['sass']);
});

gulp.task('default', ['sass', 'watch']);