var express = require('express');
var app = express();

var env = process.env.NODE_ENV = process.env.NODE_ENV || 'development';

if (process.env.OPENSHIFT_NODEJS_PORT) {
    env = process.env.NODE_ENV = 'production';
}

var Utilities = require('./server/Utilities');

// load configuration
var config = require('./server/config/config')[env];

//console.log(config.rootPath);

// connect to database
require('./server/db/mongoose')(config, function done(err, db) {
	// set DB in the global
	global.db = db;

	// set the Config in the global scope.
	global.config = config;

	//console.log('done: ' + err + ' and ' + db);
	if(err) {
		console.log('Failed to connect to DB. Error: ' + err);
		return;
	}


	// bootstrap express application
	require('./server/config/express')(app, config, db);

	// start server
	app.listen(config.port, config.ip_address, function (err) {
	    if (err) {
	        console.log('Node server failed to start! Error = ' + err);
	        return;
	    }

	    console.log("Node server running at http://localhost:" + config.port);
	});
});
