var express = require('express'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    session = require('express-session'),
    fs = require('fs'),
    MongoStore = require('connect-mongo')(session),
    _ = require('lodash'),
    async = require('async'),
    RouteHandlers = require('../http/RouteHandlers'),
    favicon = require('serve-favicon'),
    compression = require('compression');

module.exports = function (app, config, db) {

    app.use(compression({ threshold: 0 }));

    app.set('port', config.port);
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: true}));
    app.use(cookieParser());

    // Set MongoDB persistent session - user receives a cookie named connect.sid
    // Authentication is based on this session.
    app.use(session({
        secret: 'fgo secret.',
        resave: true,
        saveUninitialized: true,
        cookie: {
            maxAge: 2592000000 // one month
        },
        store: new MongoStore({mongooseConnection: db.Mongoose.connection})
    }));

    // please use domain.
    app.use(require('express-domain-middleware'));

    // if we don't have this route here, when you open the root of the site you are always taken to login page
    app.get('/', RouteHandlers.getWebApp);

    app.use(function (req, res, next) {

        if(config.env == "production") {
            // add expires headers to all css and js files
            if (req.url.indexOf('.css') > -1 || req.url.indexOf('.js') > -1) {
                res.setHeader('Cache-Control', 'public, max-age=345600'); // 4 days
                res.setHeader('Expires', new Date(Date.now() + 345600000).toUTCString());
            }
        }
        return next();

    });

    app.use(favicon(__dirname + './../../app/favicon.ico', { maxAge: 2592000000 }));

    // When getting the /index.html, inject the user in the window.currentUser variable.
    app.use(express.static(config.rootPath + '/app'));

    app.use('/languages', express.static(config.rootPath + '/app/languages'));

    // add auth routes
    require('./routes')(app, config, db);

    // the asterisk route should be on the bottom, so that all the routes to be checked and if none of them applies
    // then serve the web app, this is needed because of the html mode which does not have # in the url
    app.get('*', RouteHandlers.getWebApp);

    app.use(function onError(err, req, res, next) {
        console.error(err.stack);
        res.send(500, {
            message: err.message
        });
    });
};