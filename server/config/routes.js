var RouteHandlers = require('../http/RouteHandlers');

module.exports = function(app, config, db) {
	// Local subscription
	app.post('/subscribe', RouteHandlers.postLocalSubscription);
	app.get('/activate', RouteHandlers.getActivateSubscription);
	app.get('/cities', RouteHandlers.getCities);

	app.get('/subscriptions/:magic', RouteHandlers.getAllSubscriptions);
	app.get('/subscriptions/:magic/count', RouteHandlers.getAllSubscriptionsCount);
	// Not Found
	//app.get('*', RouteHandlers.notFound);
};