var path = require('path');
var rootPath = path.normalize(__dirname + '../../../');
exports = module.exports = {
	development: {
		env: "development",
		port: process.env.PORT || 3000,
        ip_address: process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1',
		site_address: "http://localhost:3000/",
		rootPath: rootPath,
		dbConnString: 'mongodb://localhost/fgo'
	},
	production: {
		env: "production",
        port: process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 3000,
        ip_address: process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1',
		site_address: "http://www.fgo.io/",
		rootPath: rootPath,
		dbConnString: 'mongodb://admin:gLXSQc_EFKQT@' + process.env.OPENSHIFT_MONGODB_DB_HOST + ':' + process.env.OPENSHIFT_MONGODB_DB_PORT + '/', // openshift mongo
	}
};