var mongoose = require('mongoose');
require('mongoose-double')(mongoose);

exports = module.exports = function(config, done) {
	mongoose.connect(config.dbConnString);
	
	var Subscription = require('./Subscription')(mongoose);

	var dbObject = {
		Subscription: Subscription,
		Mongoose: mongoose,
		Schema: mongoose.Schema
	};

	var db = mongoose.connection;
	
	db.on('error', function callback(err) {
		console.error.bind(console, 'connection error: ' + err);

		done(err);
	});

	db.once('open', function callback() {
	  console.log('Connected to DB');

	  done(null, dbObject); // ok
	});
};