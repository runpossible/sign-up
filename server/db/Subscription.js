exports = module.exports = function(mongoose) {

    var subscriptionSchema = new mongoose.Schema({
        email: {
            type: String,
            unique: true
        },
        firstName: String,
        lastName: String,
        city: String,
        age: Number,
        plays: Number,
        confirmationCode: String,
        confirmed: Boolean,
        newsletters: Boolean,
        ipAddress: String,
        updatedAt: Date
    });

    var Group = mongoose.model('Subscription', subscriptionSchema);

    // Automatically set the updated at
    subscriptionSchema.pre('save', function(next) {
        this.updatedAt = new Date();
        next();
    });

    return Group;
};