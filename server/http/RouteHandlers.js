/* global db, config */
'use strict';

var fs = require('fs'),
    cheerio = require('cheerio'),
    _ = require('lodash'),
    async = require('async'),
    db = global.db,
    config = global.config,
    rand = require("generate-key"),
    nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport("SMTP", {
    host: "mail.privateemail.com",
    port: "25",
    auth: {
        user: "contact@fgo.io",
        pass: "fgoDumDeeDum123"
    }
});

var RouteHandlers = {};

var mailContent = fs.readFileSync(config.rootPath + '/app/activation-mail.html', 'utf8');
var cities = fs.readFileSync(config.rootPath + '/app/languages/cities.json', 'utf8');

// Static methods
RouteHandlers.respond = function respond(res, data, httpStatusCode, contentType) {
    httpStatusCode = httpStatusCode || 200;
    contentType = contentType || 'application/json; charset=utf-8';

    res.charset = 'utf-8';
    res.writeHead(httpStatusCode, {'Content-Type': contentType});
    res.end(JSON.stringify(data));
};

// Not Found
RouteHandlers.notFound = function notFound(req, res) {
    RouteHandlers.respond(res, { error: _.template('Cannot <%= method %> <%= originalUrl %>')(req)}, 404);
};

// Web
RouteHandlers.getWebApp = function getWebApp(req, res) {
    var html = fs.readFileSync(config.rootPath + '/app/index.html', 'utf8');
    var $ = cheerio.load(html);

    if (process.env.OPENSHIFT_NODEJS_PORT) {
        // use r.js optimized file
        $('script[data-main="/main"]').attr('data-main', '/main-built.js?bust=' + global.jsSuffix);

        var ga = fs.readFileSync(config.rootPath + '/app/js/ga/google-tracking.js', 'utf8');
        var analyticsScript = '<script>' + ga + '</script>';
        $('body').append(analyticsScript);
    }
    res.send($.html({decodeEntities: false}));
};

// Local subscription
RouteHandlers.postLocalSubscription = function postLocalSubscription(req, res){
    var searchEmail = req.body.email.trim();
    db.Subscription.find({ email: searchEmail }).exec(function (err, existing) {
        var subscription;
        var isNew = false;
        if(existing && existing.length > 0)
        {
            subscription = existing[0];
        }
        else
        {
            subscription = new db.Subscription();
            subscription.email = searchEmail;
            isNew = true;
        }
        subscription.firstName = req.body.fname;
        subscription.lastName = req.body.lname;
        subscription.city = req.body.city;
        subscription.age = req.body.age;
        subscription.plays = req.body.plays;
        subscription.newsletters = req.body.newsletters;
        subscription.ipAddress = req.headers['x-forwarded-for'] ||
            req.connection.remoteAddress ||
            req.socket.remoteAddress ||
            req.connection.socket.remoteAddress;

        if(isNew || !subscription.confirmed)
        {
            subscription.confirmationCode = rand.generateKey(7);
            subscription.confirmed = false;

            var $ = cheerio.load(mailContent);
            var navigateUrl = global.config.site_address + 'activate?email=' + searchEmail + '&code=' + subscription.confirmationCode;
            $("a[href='activation']").attr('href', navigateUrl);

            var mailOptions = {
                from: 'Football Game Oranizer <contact@fgo.io>', // sender address
                to: searchEmail, // list of receivers
                subject: 'FGO: Моля, потвърдете абонирането', // Subject line
                html: $.html() // html body
            };

            // send mail with defined transport object
            transporter.sendMail(mailOptions, function(error, info){
                if(error){
                    return console.log(error);
                }
                console.log('Message sent: ' + info.response);

            });
        }

        subscription.save(function (err, savedSubscription) {
            if (err) {
                console.log(err);
                return RouteHandlers.respond(res, err, 500);
            }
            else
            {
                return RouteHandlers.respond(res, 'Subscription Received', 200);
            }
        });
    });
};

RouteHandlers.getAllSubscriptions = function getAllSubscriptions(req, res) {
    var magic = req.params.magic;
    if(magic !== 'fgo-connecting-people-to-play-the-most-popular-game') {
        return RouteHandlers.respond(res, 'Unauthorized', 500);
    }

    db.Subscription.find({ }).sort('-updatedAt').exec(function (err, subscriptions) {
        if(err) {
            console.log(err);
            return RouteHandlers.respond(res, err, 500);
        }

        return RouteHandlers.respond(res, subscriptions, 200);
    });
};

RouteHandlers.getAllSubscriptionsCount = function getAllSubscriptionsCount(req, res) {
    var magic = req.params.magic;
    if(magic !== 'fgo-connecting-people-to-play-the-most-popular-game') {
        return RouteHandlers.respond(res, 'Unauthorized', 500);
    }

    db.Subscription.find({ }).count(function (err, subscriptionsCount) {
        if(err) {
            console.log(err);
            return RouteHandlers.respond(res, err, 500);
        }

        return RouteHandlers.respond(res, { SubscriptionsCount: subscriptionsCount }, 200);
    });
};

RouteHandlers.getActivateSubscription = function getActivateSubscription(req, res) {
    var searchEmail = req.query.email.trim();
    db.Subscription.find({ email: searchEmail }).exec(function (err, existing) {
        if(existing && existing.length > 0)
        {
            var subscription = existing[0];
            if(!subscription.confirmed && subscription.confirmationCode === req.query.code.trim())
            {
                subscription.confirmed = true;
                subscription.save(function (err, savedSubscription) {
                    if (err) {
                        console.log(err);
                    }
                    res.redirect('/thank-you?email=' + searchEmail);
                });
                return;
            }
        }
        res.redirect('/thank-you?email=' + searchEmail);
    });
};

RouteHandlers.getCities = function getCities(req, res) {
    return RouteHandlers.respond(res, JSON.parse(cities));
};

exports = module.exports = RouteHandlers;